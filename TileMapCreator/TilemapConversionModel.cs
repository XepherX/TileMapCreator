﻿using System.Drawing;

namespace TileMapCreator
{
    public class TilemapConversionModel
    {
        public Bitmap TileMap { get; set; }
        public Bitmap TileSet { get; set; }
        public byte[] TileIndicies { get; set; }
    }
}
