﻿using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;

namespace TileMapCreator
{
    public class Tile
    {
        public const int Width = 8;
        public const int Height = 8;
        private bool _hasBitmapCached = false;
        private Bitmap _bitmap = null;

        public Bitmap Bitmap
        {
            get
            {
                if (_hasBitmapCached)
                    return _bitmap;

                _bitmap = this.BytesAsBitmap();
                _hasBitmapCached = true;
                return _bitmap;
            }
        }

        private Bitmap BytesAsBitmap()
        {
            var bitmap = new Bitmap(Width, Height, PixelFormat.Format24bppRgb);
            var memoryRegion = bitmap.LockBits(new Rectangle(0, 0, Width, Height), ImageLockMode.WriteOnly,
                PixelFormat.Format24bppRgb);

            Marshal.Copy(ImageBytes, 0, memoryRegion.Scan0, ImageBytes.Length);
            bitmap.UnlockBits(memoryRegion);
            return bitmap;
        }

        public byte[] ImageBytes { get; set; }

        public Tile(byte[] imageBytes)
        {
            ImageBytes = imageBytes;
        }


    }
}
