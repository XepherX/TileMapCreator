﻿using System.Collections.Generic;
using System.Drawing;

namespace TileMapCreator
{
    public class PaletteBuilder
    {
        public void BuildPalette(Bitmap tileSet)
        {
            List<Color> kulers = new List<Color>();

            var tilesetBytes = tileSet.AsBytes(tileSet.SizeAsRectangle());

            for (int i = 0; i < tilesetBytes.Length; i += 3)
            {
                var col = Color.FromArgb(0, tilesetBytes[i], tilesetBytes[i + 1], tilesetBytes[i + 2]);
                if (!kulers.Contains(col))
                    kulers.Add(col);

            }

            var test = kulers;
        }
    }
}
