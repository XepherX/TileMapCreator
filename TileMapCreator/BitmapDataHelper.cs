﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System.Text;

namespace TileMapCreator
{
    public static class BitmapDataHelper
    {
        private static int debugSaveCounter;
        public static void DebugSave(this Bitmap bitmap)
        {
            debugSaveCounter++;
            bitmap.Save($"C:\\users\\ptvs-ff\\desktop\\debug_{debugSaveCounter}.bmp",ImageFormat.Bmp);
        }

        public static Rectangle SizeAsRectangle(this Bitmap bitmap)
        {
            return new Rectangle(0, 0, bitmap.Width, bitmap.Height);
        }

        public static byte[] AsBytes(this Bitmap bitmap, Rectangle area)
        {
            var memoryRegion = bitmap.LockBits(area, ImageLockMode.ReadOnly, bitmap.PixelFormat);

            var absStride = Math.Abs(memoryRegion.Stride);
            var retArr = new byte[memoryRegion.Width * memoryRegion.Height * 3];
            unsafe
            {
                var linePointer = (byte*)memoryRegion.Scan0;
                if (linePointer == null) throw new NullReferenceException(nameof(linePointer));

                for (var j = 0; j < memoryRegion.Height; j++)
                {
                    var lineOffsetPointer = linePointer;
                    for (var i = 0; i < memoryRegion.Width; i++)
                    {

                        retArr[i * 3 + j * memoryRegion.Width * 3 + 0] = *lineOffsetPointer++;
                        retArr[i * 3 + j * memoryRegion.Width * 3 + 1] = *lineOffsetPointer++;
                        retArr[i * 3 + j * memoryRegion.Width * 3 + 2] = *lineOffsetPointer++;
                    }

                    linePointer += absStride;
                }
            }
            bitmap.UnlockBits(memoryRegion);
            return retArr;
        }

        [DllImport("msvcrt.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern int memcmp(byte[] b1, byte[] b2, UIntPtr count);

        public static bool IsEqual(this byte[] bytesLeft, byte[] bytesRight)
        {
            return memcmp(bytesLeft, bytesRight, new UIntPtr((uint)bytesLeft.Length)) == 0;
        }

        public static string ToTable(this int[,] bools)
        {
            var retstr = new StringBuilder();
            for (var i = 0; i <= bools.GetUpperBound(0); i++)
            {
                for (var j = 0; j <= bools.GetUpperBound(1); j++)
                {
                    retstr.Append(bools[i, j].ToString("X").PadLeft(2, '0') + " ");
                }

                retstr.AppendLine();
            }

            return retstr.ToString();
        }
    }
}
