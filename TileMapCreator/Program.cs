﻿using System;
using System.Windows.Forms;

namespace TileMapCreator
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {

            var test = GBAColor.FromRGB(87, 143, 8).ToGBAHexString();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }
    }
}
