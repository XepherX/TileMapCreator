﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;

namespace TileMapCreator
{
    public class TileSetBuilder
    {
        public const int TileSetLimitWidth = 16;
        public const int TileSetLimitHeight = 20;
        private readonly TilemapConversionModel _conversionModel;

        public TileSetBuilder(TilemapConversionModel conversionModel)
        {
            _conversionModel = conversionModel;
        }

        public void BuildTileset()
        {
            List<Tile> tilesetList = new List<Tile>();
            var tileSetIndicies = new int[_conversionModel.TileMap.Height / Tile.Height, _conversionModel.TileMap.Width / Tile.Width];
            var tileIndex = 1;
            for (var y = 0; y < _conversionModel.TileMap.Height / Tile.Height; y++)
            {
                for (var x = 0; x < _conversionModel.TileMap.Width / Tile.Width; x++)
                {
                    if (tileSetIndicies[y, x] > 0) continue;
                    var copyRegion =
                        _conversionModel.TileMap.AsBytes(new Rectangle(x * Tile.Width, y * Tile.Height, Tile.Width, Tile.Height));
                    var copyTile = new Tile(copyRegion);
                    tilesetList.Add(copyTile);

                    for (var lookaheadY = 0; lookaheadY < _conversionModel.TileMap.Height / Tile.Height; lookaheadY++)
                    {
                        for (var lookaheadX = 0; lookaheadX < _conversionModel.TileMap.Width / Tile.Width; lookaheadX++)
                        {
                            if (tileSetIndicies[lookaheadY, lookaheadX] > 0) continue;
                            var compareRegion = _conversionModel.TileMap.AsBytes(new Rectangle(lookaheadX * Tile.Width,
                                lookaheadY * Tile.Height, Tile.Width, Tile.Height));

                            if (copyRegion.IsEqual(compareRegion))
                            {
                                tileSetIndicies[lookaheadY, lookaheadX] = tileIndex;
                            }

                        }
                    }

                    tileIndex++;
                }
            }
            
            /*
             * limit is ‭4095‬ because while each tile is indexed with 2 bytes,
             * the high order nybble of the upper byte is actually used to
             * store palette information.
             *
             * 0x0FFF = ‭4095‬
             *   ^-- this nybble is used for pallette info, 0x00 through 0xF0
             */
            if (tilesetList.Count > 0x0FFF || tilesetList.Count > TileSetLimitWidth * TileSetLimitHeight)
            {
                throw new TooManyTilesException(tilesetList.Count, Math.Min(TileSetLimitWidth * TileSetLimitHeight, 0x0FFF));
            }

            _conversionModel.TileSet = RenderTilelist(tilesetList);
            _conversionModel.TileIndicies = CalculateTileSetByteIndicies(tileSetIndicies);

            

        }

        private Bitmap RenderTilelist(List<Tile> tilesetList)
        {
            var tilesetImage = new Bitmap(TileSetLimitWidth * 8, TileSetLimitHeight * 8, PixelFormat.Format24bppRgb);
            using (var graphics = Graphics.FromImage(tilesetImage))
            {
                for (var i = 0; i < tilesetList.Count; i++)
                {
                    var target = new Rectangle(i * Tile.Width % (TileSetLimitWidth * Tile.Width), i / TileSetLimitWidth * Tile.Height,
                        Tile.Width, Tile.Height);
                    graphics.DrawImage(tilesetList[i].Bitmap, target);
                }
            }

            return tilesetImage;
        }

        private byte[] CalculateTileSetByteIndicies(int[,] tileSetIndicies)
        {
            var tileSetIndiciesResult = new List<byte>();
            foreach (var tile in tileSetIndicies)
            {
                ushort target = (ushort)(tile - 1);
                target |= 0x00 << 8;
                var testo = BitConverter.GetBytes(target);
                tileSetIndiciesResult.AddRange(testo);
            }
            return tileSetIndiciesResult.ToArray();
        }
    }

    public class TooManyTilesException : Exception
    {
        public int TileCount { get; }
        public int MaxAllowedCount { get; }

        public TooManyTilesException(int tilesetListCount, int maxAllowedCount)
        {
            TileCount = tilesetListCount;
            MaxAllowedCount = maxAllowedCount;
        }
    }
}
