﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TileMapCreator
{
    public partial class MainForm : Form
    {

        public TilemapConversionModel ConversionModel { get; set; } = new TilemapConversionModel();
        public TileSetBuilder TileSetBuilder { get; set; }
        public PaletteBuilder PaletteBuilder { get; set; } 

        public MainForm()
        {
            InitializeComponent();

            TileSetBuilder = new TileSetBuilder(ConversionModel);
            PaletteBuilder = new PaletteBuilder();

            typeof(PictureBox).InvokeMember("DoubleBuffered",
                BindingFlags.SetProperty | BindingFlags.Instance | BindingFlags.NonPublic,
                null, pictureBox1, new object[] { true });

            typeof(GroupBox).InvokeMember("DoubleBuffered",
                BindingFlags.SetProperty | BindingFlags.Instance | BindingFlags.NonPublic,
                null, groupBox1, new object[] { true });

        }

        private async Task LoadImage(Bitmap selectedImage)
        {
            await Task.Run(() =>
            {
                ConversionModel.TileMap = selectedImage.Clone(new Rectangle(0, 0, selectedImage.Width, selectedImage.Height), PixelFormat.Format24bppRgb);
                TileSetBuilder.BuildTileset();
            });

            pictureBox1.Image = ConversionModel.TileMap;
            pictureBox2.Image = ConversionModel.TileSet;
            exportSubMenuMenuItem.Enabled = true;
        }

        private async void convertImageToolStripMenuItem_Click(object sender, EventArgs e)
        {


            bool imageSuccessfullyPicked = false;
            Bitmap selectedImage = null;
            do
            {
                var inputFileDialog = new OpenFileDialog
                {
                    Filter = "Images (*.png, *.bmp)|*.png; *.bmp"
                };

                var result = inputFileDialog.ShowDialog();
                if (result != DialogResult.OK) return;

                selectedImage = (Bitmap)Image.FromFile(inputFileDialog.FileName);
                if (selectedImage.Width % Tile.Width != 0 || selectedImage.Height % Tile.Height != 0)
                {
                    var cancelResult = MessageBox.Show(
                        $"The chosen image can't be evenly tiled with {Tile.Width}x{Tile.Height} tiles. Image is {selectedImage.Width}x{selectedImage.Height}",
                        "Error!",
                        MessageBoxButtons.RetryCancel, MessageBoxIcon.Error);
                    if (cancelResult == DialogResult.Cancel) return;
                }
                else
                {
                    imageSuccessfullyPicked = true;
                }

            } while (!imageSuccessfullyPicked);



            try
            {
                await LoadImage(selectedImage);
                PaletteBuilder.BuildPalette(ConversionModel.TileSet);
            }
            catch (TooManyTilesException tooManyTiles)
            {
                MessageBox.Show(
                    $"The given image was too complex. {tooManyTiles.TileCount} tiles were built, allowed max is 270.",
                    "Too many tiles", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }





        }

        private void MainForm_Load(object sender, EventArgs e)
        {

        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void exportTilesetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var location = ChooseSaveLocation("Bitmap", "*.bmp");
            if (location == null) return;
            ConversionModel.TileSet.Save(location, ImageFormat.Bmp);
            MessageBox.Show($"Successfully exported TileSet to {location}", "Success", MessageBoxButtons.OK,
                MessageBoxIcon.Information);


        }

        private void exportTileIndiciesrawToolStripMenuItem_Click(object sender, EventArgs e)
        {

            var result = MessageBox.Show(
                "When creating the RAW Tile Indicies for FR/LG worldmaps, the file must be padded to 1216 bytes.\n\nPad files to 1216 bytes?",
                "RAW Tile Indicies padding", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);

            if (result == DialogResult.Cancel) return;


            var location = ChooseSaveLocation("RAW Tile Indicies", "*.raw");
            if (location == null) return;
            using (var fileHandle = File.OpenWrite(location))
            {
                fileHandle.Write(ConversionModel.TileIndicies, 0, ConversionModel.TileIndicies.Length);
                if (result == DialogResult.Yes)
                {
                    var padding = 1216 - fileHandle.Length;
                    var paddingArr = new byte[padding];
                    if (padding > 0)
                        fileHandle.Write(paddingArr, 0, paddingArr.Length);
                }
            }


            MessageBox.Show($"Successfully exported RAW Tile Indicies to {location}", "Success", MessageBoxButtons.OK,
                MessageBoxIcon.Information);
        }

        private string ChooseSaveLocation(string fileDescription, string fileExtension)
        {
            var saveFileDialog = new SaveFileDialog
            {
                Filter = $"{fileDescription} ({fileExtension})|{fileExtension}"
            };
            if (saveFileDialog.ShowDialog() == DialogResult.OK) return saveFileDialog.FileName;
            return null;
        }


    }
}
