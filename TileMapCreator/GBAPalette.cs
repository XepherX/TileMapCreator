﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace TileMapCreator
{
    public struct GBAColor
    {
        public ushort Red { get; set; }
        public ushort Green { get; set; }
        public ushort Blue { get; set; }

        private static ushort ShiftedColor(ushort color)
        {
            if ((color & 0b00000111) >= 4)
            {
                return (ushort)((color & 0b11111000) + 0b1000);
            }

            return color;
        }

        public static GBAColor FromColor(Color c)
        {
            return new GBAColor
            {
                Red = ShiftedColor(c.R),
                Green = ShiftedColor(c.G),
                Blue = ShiftedColor(c.B)
            };
        }

        public static GBAColor FromRGB(ushort r, ushort g, ushort b)
        {
            return new GBAColor
            {
                Red = ShiftedColor(r),
                Green = ShiftedColor(g),
                Blue = ShiftedColor(b)
            };
        }

        public Color ToColor()
        {
            return Color.FromArgb(0, Red, Green, Blue);
        }

        public string ToGBAHexString()
        {
            ushort mergedColor = 0;
            mergedColor |= (ushort)(Red >> 3 << 0);
            mergedColor |= (ushort)(Green >> 3 << 5);
            mergedColor |= (ushort)(Blue >> 3 << 10);
            return mergedColor.ToString("X4");
        }
    }
    public class GBAPalette
    {

    }
}
